/**
 * @public
 */
export interface CSSStyleDeclaration {
	[key: string]: string;
}
